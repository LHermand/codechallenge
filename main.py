import json
import pathlib
import io


def file_to_event_list(file_path):
    """Convert the file from "file_path" to a list of dictionnaries
    Args:
        file_path (string) : path to the file to convert
    """
    with io.open(file_path, "r", encoding="utf8") as file_input:
        Lines = file_input.readlines()
        event_list = []
        for line in Lines:
            try:
                new_el = json.loads(line)
                event_list.append(new_el)
            except ValueError:
                print(
                    "the line "
                    + line[:-2]
                    + " could not be converted to a dictionnary\n"
                )
    return event_list


def event_list_to_file(event_list, file_path):
    """Convert list of dictionnaries event_list to the output file
    Args:
        event_list (list) : the list of dictionnary to write in the file
        file_path (string) : path to the file to write
    """
    try:
        with io.open(file_path, "x", encoding="utf8") as file_output:
            for event in event_list:
                file_output.write(json.dumps(event, ensure_ascii=False))
                file_output.write("\n")
    except FileExistsError:
        print(
            "Warning ! Output file already exist.\nRemove the old output file to continue"
        )
        raise


def GDPR_key_check(event_dict, key):
    """Key verification to remove sensitive data from the event given in parameter
    Args:
        event_dict (dict) : the dictionnary describing the event
        key (string) : key to check
    """
    PA_index_key = str(key).find("patient_id")
    if PA_index_key != -1:
        event_dict[key] = "[[REDACTED]]"
    ON_index_key = max(
        str(key).find("operatorname"),
        str(key).find("operatorName"),
        str(key).find("operator_Name"),
        str(key).find("operator_name"),
    )  # operatorName index in value
    if ON_index_key != -1:
        event_dict[key] = "[[REDACTED]]"


def GDPR_value_check(event_dict, key):
    """Value verification to remove sensitive data from the event given in parameter
    Args:
        event_dict (dict) : the dictionnary describing the event
        key (string) : key where the value to check is
    """
    # value verification
    value = event_dict[key]
    PA_index = str(value).find("patient_id=")  # patient_id index in value
    if PA_index != -1:
        # patient_id='<something>'
        value = (
            value[: PA_index + 10]
            + "='[[REDACTED]]'"
            + value[value[PA_index + 12 :].find("'") + PA_index + 13 :]
        )
    ON_index = max(
        str(value).find("operatorname"),
        str(value).find("operatorName"),
        str(value).find("operator_Name"),
        str(value).find("operator_name"),
    )  # operatorName index in value
    if ON_index != -1:
        # operatorName: [<something>]
        value = (
            value[:ON_index]
            + "operatorName: [[REDACTED]]"
            + value[ON_index:].split("]")[1]
        )
    event_dict[key] = value


def GDPR_event_list(event_list):
    """Remove sensitive data from the event list given in parameter
    Args:
        event_list (list) : the list of dictionnaries describing the events
    """
    for event in event_list:
        for key in event:
            GDPR_key_check(event, key)
            GDPR_value_check(event, key)


if __name__ == "__main__":
    path_location = str(pathlib.Path().absolute())
    # retrieve the data to be processed
    event_list = file_to_event_list(path_location + "/input.txt")
    # Remove sensitive data
    GDPR_event_list(event_list)
    # Write the GDPR friendly data in a new file
    event_list_to_file(event_list, path_location + "/output.txt")
