### Abstract
Our company would like to collect data from our proton therapy centers.
You are in charge of the development of a part of the program.
The part you develop must be able to read a file containing (presumably) json events. There is one event per line.

The program must remove sensitive data so that the company is GDPR compliant.

The output of the processing must be saved in another file.

EVERY content related to patient ID (patient_id='<something>') or the name of an operator (operatorName: [<something>]) must be anonymized 
(replaced with the content <something>: [[REDACTED]]).

The program must also handle errors properly in case a JSON event cannot be processed and log an error message.

### Examples


    {"key1": "value1", "key2": "some logging text patient_id='Donald Duck'"}
     --> 
    {"key1": "value1", "key2": "some logging text patient_id='[[REDACTED]]'"}

    {"data": "some fancy log message", "content": "The employee with operatorName: [John Doe] entered the room" }
    --> 
    {"data": "some fancy log message", "content": "The employee with operatorName: [[REDACTED]] entered the room" }
